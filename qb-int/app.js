'use strict'
const axios = require('axios');
const axiosRetry = require('axios-retry');
const OAuthClient = require('intuit-oauth');
const path = require('path');

const CSVToJSON = require('csvtojson');
const fs = require('fs');
const key = fs.readFileSync('./certs/key.pem');
const cert = fs.readFileSync('./certs/cert.pem');
const https = require('https');
const csvjson = require('csvjson');
const writeFile = require('fs').writeFile;

var http = require('http');
var port = process.env.PORT || 3001;
var request = require('request');
var qs = require('querystring');
var util = require('util');
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var session = require('express-session');
var express = require('express');

var app = express();
const server = https.createServer({ key: key, cert: cert }, app);
var QuickBooks = require('../node-qb');
var Tokens = require('csrf');
const { SSL_OP_EPHEMERAL_RSA } = require('constants');
var csrf = new Tokens();
var redirect_uri = 'https://dfp-qbo.btrsoftware.com:' + port + '/callback/';
var accessToken = '';
var qbo = null;
var awsurl = "https://9emwmvnc76.execute-api.us-west-1.amazonaws.com/prod"
const crypto = require('crypto');

QuickBooks.setOauthVersion('2.0');

server.listen(3001, () => {
  console.log('listening on 3001');
  console.log('Express server listening on port ' + app.get('port'));
  console.log('redirecturl - ' + redirect_uri);
});

// Generic Express config
app.set('port', port);
app.set('views', 'views');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser('brad'));
app.use(session({ resave: false, saveUninitialized: false, secret: 'smith' }));
app.use(express.static('public'))



/*app.listen(app.get('port'), function () {
  console.log('Express server listening on port ' + app.get('port'));
  console.log('redirecturl - ' + redirect_uri);
});*/

// INSERT YOUR CONSUMER_KEY AND CONSUMER_SECRET HERE

//sandbox
//var consumerKey = 'AB9oJtvuKe333wJt7qbLhwpSnG8jjCvsdWjWGLM5Ly4ZU72NG1',
//  consumerSecret = '4i6zmYlN3jrUBsmEFMLUoN6PSNTDUyEWPMcPtNmV';

//prod
var consumerKey = 'ABnRbya3XX60qveOp0iVNSJmDOJH8886tEewvvBvWIpkOEPpiP',
  consumerSecret = 'U64fV0ngwt4vHXnr8cCxG9JEECifd6t1ClmBBCKd';

const oauthClient = new OAuthClient({
  clientId: consumerKey,
  clientSecret: consumerSecret,
  environment: 'production',
  redirectUri: redirect_uri,
});

const authUri = oauthClient.authorizeUri({
  scope: [OAuthClient.scopes.Accounting, OAuthClient.scopes.OpenId],
  state: 'testState',
});

app.get('/', function (req, res) {
  //res.render('index');
  res.redirect('/start');
});

app.get('/start', function (req, res) {
  res.render('intuit.ejs', { port: port, appCenter: QuickBooks.APP_CENTER_BASE });
});

// OAUTH 2 makes use of redirect requests
function generateAntiForgery(session) {
  session.secret = csrf.secretSync();
  return csrf.create(session.secret);
};

app.get('/requestToken', function (req, res) {
  var redirecturl = QuickBooks.AUTHORIZATION_URL +
    '?client_id=' + consumerKey +
    '&redirect_uri=' + encodeURIComponent(redirect_uri) +  //Make sure this path matches entry in application dashboard
    '&scope=com.intuit.quickbooks.accounting' +
    '&response_type=code' +
    '&state=' + generateAntiForgery(req.session);

  res.redirect(redirecturl);
});

app.get('/site-list', function (req, res) {
  if (qbo == null) {
    res.redirect('/start');
  } else {
    res.render('sites.ejs', { port: port, appCenter: QuickBooks.APP_CENTER_BASE });
  }
});

app.get('/site-list/data', function (req, res) {
  if (qbo == null) {
    res.redirect('/start');
  } else {
    axios.get(awsurl + '/sites')
      .then(response => {
        //console.log(response.data.data);
        res.send(response.data.data);
      })
      .catch(error => {
        console.log(error);
      });
  }
});

app.get('/run-results', function (req, res) {
  if (qbo == null) {
    res.redirect('/start');
  } else {
    //console.log("query: ", req.query);
    res.render('results.ejs', { runkey: req.query.runkey });
  }
});

app.get('/run-results/data', function (req, res) {
  if (qbo == null) {
    res.redirect('/start');
  } else {
    //console.log("requrest: ", req);
    axios.get(awsurl + '/runresults?runkey=' + req.query.runkey)
      .then(response => {
        //console.log(response.data.data);
        res.send(response.data.data);
      })
      .catch(error => {
        console.log(error);
      });
  }
});

app.get('/bills', function (req, res) {
  qbo.findBills(function (_, bills) {
    bills.QueryResponse.Bill.forEach(function (account) {
      console.log(account.DueDate);
      console.log(account.VendorRef.name);
    });
  });
});

app.get('/invoices', function (req, res) {

  if (qbo == null) {
    res.redirect('/start');
  } else {
    qbo.findInvoices({ DocNumber: '1' }, function (_, invoices) {
      if (Object.keys(invoices.QueryResponse).length === 0) {
        //console.log("no results");
        res.send();
      } else {
        //console.log("invocies:", invoices);
        invoices.QueryResponse.Invoice.forEach(function (invoice) {
          //console.log(invoice);
          //console.log(account.VendorRef.name);
        });
      }
    });
  }
});

app.get('/customer', function (req, res) {
  if (qbo == null) {
    res.redirect('/start');
  } else {
    qbo.findCustomers(function (_, customers) {
      if (Object.keys(customers.QueryResponse).length === 0) {
        console.log("no results");
        res.send();
      } else {
        console.log("customers:", customers);
        customers.QueryResponse.Customer.forEach(function (customer) {
          console.log(customer);
        });
      }
    });
  }
});

app.get('/orders', function (req, res) {
  if (qbo == null) {
    res.redirect('/start');
  } else {
    res.render('orders.ejs', { site_name: req.query.site_name, invoicepre: req.query.invoicepre, domain: req.query.domain, token: req.query.token, lastrun: req.query.lastrun, todate: req.query.todate });
  }
});

app.get('/items', function (req, res) {
  if (qbo == null) {
    res.redirect('/start');
  } else {
    res.render('items.ejs');
  }
});


async function getorders(req) {
  return new Promise(resolve => {
    var date = req.query.cdate;
    var todate = req.query.todate;
    //filtered by date
    //get order headers
    var ordersurl = "https://" + req.query.domain + ".mybrightsites.com/api/v2.3.0/orders?token="
      + req.query.token + "&created_at_from=" + date + "&created_at_to=" + todate + "&per_page=500";
    //filtered by status
    //var ordersurl = "https://" + req.query.domain + ".mybrightsites.com/api/v2.3.0/orders?token=" + req.query.token + "&status=shipped";

    axiosRetry(axios, {
      retries: 10, // number of retries
      retryDelay: (retryCount) => {
        console.log(`retry attempt: ${retryCount}`);
        return retryCount * 2000; // time interval between retries
      },
      retryCondition: (error) => {
        // if retry condition is not specified, by default idempotent requests are retried
        return error.response.status === 500;
      },
    });

    console.log("getting orderlist", ordersurl);
    axios.get(ordersurl)
      .then(response => {
        //console.log("Response: ", response);
        if (response.data.orders.length > 0) {
          var orders = response.data.orders;

          //get tx status
          var getstatus = new Promise((resolve, reject) => {

            orders.forEach((order, index, array) => {

              var statusurl = awsurl + "/txstatus?order_id=" + order.order_id + "&site_name=" + req.query.site_name;
              //console.log("statusurl", statusurl);
              //console.log("getting status: " +  order.order_id);
              axios.get(statusurl)
                .then(response => {
                  order.txstatus = response.data.data.txstatus;
                  order.status_date = response.data.data.status_date;
                  //check for order in QBO
                  var qboorder = req.query.invoicepre + "" + order.order_id;
                  //console.log("checking for order:", qboorder);
                  qbo.findInvoices({ DocNumber: qboorder }, function (_, invoices) {
                    //console.log(invoices[0]);
                    if (invoices.fault != null) {
                      console.log("invoice", invoices.fault.error);
                    }

                    if (Object.keys(invoices[0].QueryResponse).length === 0) {
                      order.qbostatus = "New";
                    } else {
                      order.qbostatus = "Existing";

                    };
                  });

                  if (index === array.length - 1) resolve();  //resolve getstatus
                  //   });
                }).catch(error => {
                  console.log("error order " + order.order_id, error);
                  order.qbostatus = "Unknown";
                  resolve();
                });
            });
          });
          //get order details
          getstatus.then(() => {
            var getdetail = new Promise((resolve, reject) => {
              var index = 0;
              for( const order of orders) {
                var detailurl = "https://" + req.query.domain + ".mybrightsites.com/api/v2.3.0/orders/" + order.order_id + "?token=" + req.query.token;
                //console.log("getting detail: " +  order.order_id);  
                axios.get(detailurl)
                  .then(response => {
                    
                    order.details = response.data;
                    console.log("order details:", order.order_id);
                    //console.log("discounts:", order.details.coupons_adjustments);
                    //check for tax
                    var taxable = "N";
                    var discount = "N";
                    //console.log(order.order_id, order.details.required_adjustments);
                    order.details.required_adjustments.forEach((adj, index, array) => {
                      if(adj.note == "sales tax" && adj.amount > 0) {
                        taxable = "Y";
                      }
                    });
                    order.taxable = taxable;
                    //has discount?
                    if (order.details.coupons_adjustments.length > 0) {
                      console.log("discount found", order.details.coupons_adjustments);
                      discount = "Y";
                      }
                    order.discounts = discount;
                    index++;
                    //console.log("data", response.data);
                  }).finally(() => {
                    //console.log("index array.length",index, orders.length );
                   if (index === orders.length) resolve(); //resolve getdetail
                  });
                  
              };
            });
            getdetail.then(() => {
              resolve(orders);
            });
          });
        } else {
          //console.log("No records found");
          resolve(null);
        }
      })
      .catch(error => {
        console.log(error);
      });
  });
}

async function getdetail(req, order) {
 // console.log("in getdetail");
  return new Promise(async resolve => {
    var detailurl = "https://" + req.query.domain + ".mybrightsites.com/api/v2.3.0/orders/" + order + "?token=" + req.query.token;
    console.log("getting detail", detailurl);
    axios.get(detailurl).then(data => {
      resolve(data);
    });
  });
}

async function updatetxstatus(req, order, status, runkey, errors, issues) {
  return new Promise(async resolve => {
    var statusurl = awsurl + "/txstatus?site_name="
      + req.query.site_name + "&order_id=" + order + "&runkey=" + runkey + "&status_text=" + status + "&status_date=" + new Date().toISOString().
        replace(/T/, ' ').      // replace T with a space
        replace(/\..+/, '')
      + "&errors=" + encodeURIComponent(JSON.stringify(errors)) + "&issues=" + encodeURIComponent(JSON.stringify(issues));;
    //console.log("updating status", statusurl);
    axios.patch(statusurl).then(() => resolve()).catch(error => {
      console.log("error", error);
    });
  });

}

async function updaterun(req, runkey) {
  return new Promise(async resolve => {
    var statusurl = awsurl + "/runs?runkey="
      + runkey + "&status_date=" + new Date().toISOString().
        replace(/T/, ' ').      // replace T with a space
        replace(/\..+/, '');
    //console.log("updating status", statusurl);
    axios.patch(statusurl).then(() => resolve()).catch(error => {
      console.log("error", error);
    });
  });

}

async function updatesitestatus(req, status, errors, runkey) {
  return new Promise(async resolve => {
    var sitestatusurl = awsurl + "/sites?site_name="
      + req.query.site_name + "&runkey=" + runkey + "&status_text=" + status + "&status_date=" + new Date().toISOString().
        replace(/T/, ' ').      // replace T with a space
        replace(/\..+/, '')
      + "&errors=" + encodeURIComponent(JSON.stringify(errors));
    axios.put(sitestatusurl).then(() => resolve()).catch(error => {
      console.log("error", error);
    });
  });

}

app.get('/orders/data', async function (req, res) {
  if (qbo == null) {
    res.redirect('/start');
  } else {
    await getorders(req).then(orders => {
      res.send(orders);
    });
  }

});

async function getitems(req) {
  return new Promise(async resolve => {

    const items = await CSVToJSON().fromFile('files/set_to_dnt.csv');
    resolve(items);
  });
}

app.get('/file/items', async function (req, res) {
  if (qbo == null) {
    res.redirect('/start');
  } else {
    await getitems(req).then(items => {
      res.send(items);
    });
  }

});

app.get('/statustest', function (req, res) {
  res.render('status.ejs', { site_name: req.query.site_name, invoicepre: req.query.invoicepre, domain: req.query.domain, token: req.query.token, orders: req.query.orders, cdate: req.query.cdate, todate: req.query.todate });
});

app.get('/statustest/data', async function (req, res) {
  res.writeHead(200, { 'Content-Type': 'text/event-stream' });
  for (var i = 0; i <= 10; i++) {
    var prog = i * 10;
    //console.log("prog: " + prog.toString());
    await new Promise(resolve => setTimeout(resolve, 1000));
    res.write("event: progress\n");
    res.write("data: " + prog.toString() + "\n\n");
  }
  res.end();
});

async function getconfig(site_name) {
  return new Promise(async resolve => {
    var siteconfigurl = awsurl + "/siteconfig?site_name="
      + site_name
    axios.get(siteconfigurl).then(data => {
      resolve(data);
    });
  });
}

app.get('/processorders/data', async function (req, res) {
  var siteerrors = [];
  res.writeHead(200, { 'Content-Type': 'text/event-stream' });
  //create runkey
  var runkey = crypto.randomUUID();
  //console.log(runkey);
  res.write("event: runkey\n");
  res.write("data: " + runkey + "\n\n");

  axiosRetry(axios, {
    retries: 10, // number of retries
    retryDelay: (retryCount) => {
      //console.log(`retry attempt: ${retryCount}`);
      return retryCount * 2000; // time interval between retries
    },
    retryCondition: (error) => {
      // if retry condition is not specified, by default idempotent requests are retried
      return error.response.status === 500;
    },
  });

  await updaterun(req, runkey, []).then().catch(error => {
    console.log(error);
  });

  //get class, location, and shipping
  var classid = null;
  var classname = null;
  var locationid = null;
  var locationname = null;
  var freightid = null;
  var customer = null;
  var newinvoice = null;
  //get site config
  //console.log("getting site config");
  await getconfig(req.query.site_name).then(data => {
    //console.log(data);
    classname = data.data.data.default_class;
    locationname = data.data.data.default_location;
    customer = data.data.data.customer;
    //console.log("config", classname, locationname, customer);
  });
  //get freight id
  //console.log("getting other config");
  await new Promise(async (resolve, reject) => {
    await qbo.findItems({ Name: "Freight" }, async function (_, products) {
      freightid = products[0].QueryResponse.Item[0].Id;
      //console.log("Freight id", freightid);
    });
    resolve();
  });
  //get department id
  await new Promise(async (resolve, reject) => {
    await qbo.findDepartments({ FullyQualifiedName: locationname }, async function (_, products) {
      // console.log("Departments", products[0].QueryResponse.Department);
      locationid = products[0].QueryResponse.Department[0].Id;
      //console.log("Department id", locationid);
    });
    resolve();
  });
  //get class id
  await new Promise(async (resolve, reject) => {
    await qbo.findClasses({ FullyQualifiedName: classname }, async function (_, products) {
      //console.log("Classes", products[0].QueryResponse.Class);
      classid = products[0].QueryResponse.Class[0].Id;
      //console.log("Class id", classid);
    });
    resolve();
  });

  //get order list
  var uploadorders = JSON.parse(req.query.orders);
  //console.log("Orders to Upload", uploadorders);
  var searchdate = req.query.cdate;
  var todate = req.query.todate;
  //console.log("search date" , searchdate);

  //var ordersurl = "https://" + req.query.domain + ".mybrightsites.com/api/v2.3.0/orders?token=" + req.query.token +
  //  "&created_at_from=" + searchdate + "&created_at_to=" + todate + "&per_page=500";
  //console.log("getting orderlist", ordersurl);

  var orders = JSON.parse(req.query.orders);
  var orderindex = 0;
  //remove unchecked orders
 
  //console.log("got orders", orders.length);
  res.write("event: progress\n");
  res.write("data: " + orders.length.toString() + "\n\n");
  //get tx status
  var getstatus = new Promise(async (resolve, reject) => {
    var index = 0;
    for (const order of orders) {
      var errors = [];
      var issues = [];
      //get detail
      var detail = null
      await getdetail(req, order).then(data => {
        detail = data.data;
      });
      console.log(detail);
      //check existing order
      var qboorder = req.query.invoicepre + "" + order;
      //console.log("checking for order:", qboorder);
      await new Promise(async (resolve, reject) => {
        qbo.findInvoices({ DocNumber: qboorder }, async function (_, invoices) {
          if (Object.keys(invoices[0].QueryResponse).length === 0) {
            //console.log("Invoice not in QBO");

          } else {
            //console.log("invocies:", invoices);
            invoices[0].QueryResponse.Invoice.forEach(function (invoice) {
              //console.log(invoice);
              //console.log(account.VendorRef.name);
              errors.push({ message: "existing invoice", detail: qboorder });
            });
          }
          resolve();
        });
      }); //--promise
      //check customer
      var custnum = null;
      await new Promise(async (resolve, reject) => {
        await qbo.findCustomers({ FullyQualifiedName: customer }, async function (_, invoices) {
          if (Object.keys(invoices[0].QueryResponse).length === 0) {
            //console.log("no results - create");
            //create customer
            var params = {
              "FullyQualifiedName": detail.billing_contact.first_name,
              "PrimaryEmailAddr": {
                "Address": detail.billing_contact.email,
              },
              "DisplayName": detail.billing_contact.first_name,
              "PrimaryPhone": {
                "FreeFormNumber": detail.billing_contact.phone
              },
              "CompanyName": detail.billing_contact.first_name,
              "BillAddr": {
                "CountrySubDivisionCode": detail.billing_address.state,
                "City": detail.billing_address.city,
                "PostalCode": detail.billing_address.zip,
                "Line1": detail.billing_address.first_address,
                "Line2": detail.billing_address.second_address,
                "Country": "USA"
              },
            };
            //console.log("custparam", params);
            qbo.createCustomer(params, async function (_, customer) {
              //console.log("New customer", customer);
              custnum = customer[0].Id;
            });
          } else {
            //console.log("Customer found");
            custnum = invoices[0].QueryResponse.Customer[0].Id;
            //console.log("Custnum", custnum);
          }
          resolve();
        });
      }); // --promise
      //create invoice in qbo
      //check for tax
      var taxamt = 0;
      var shiptax = "NON";
      if (detail.required_adjustments) {
        console.log("adjustments", detail.required_adjustments);
        for (var adj of detail.required_adjustments) {
          var name = "";
          if (adj.name != undefined) {
            name = adj.name;
          } else {
            name = adj.note;
          }
          console.log("adj",adj);
          console.log("name", name);
          if (name == "sales tax") {
            taxamt += Number(adj.amount);
          }
          if (name == "shipping sales tax" && Number(adj.amount) != 0) {
            taxamt += Number(adj.amount);
            shiptax = "TAX";
          }
        }
      }
      var taxstring = 'TAX';
      if (taxamt == 0) {
        taxstring = 'NON';
      }
      //find item
      var line_items = [];
      for (const item of detail.line_items) {
        //console.log("Checking for item");
        await new Promise(async (resolve, reject) => {
          var prodid = 0;
          await qbo.findItems({ Name: item.final_sku }, async function (_, products) {
            var itemdesc = "";
            if (Object.keys(products[0].QueryResponse).length === 0) {
              itemdesc = item.final_sku + " : " + item.name;
              //console.log("Product not found");
              //log issue and change to Store Sales
              issues.push({ issue: "Product Not Found", orderid: order, product: item.final_sku })
              item.final_sku = "Store Sales";
              await new Promise(async (resolve, reject) => {
                await qbo.findItems({ Name: "Store Sales" }, async function (_, products) {
                  prodid = products[0].QueryResponse.Item[0].Id;
                  resolve();
                });
              });
            } else {
              prodid = products[0].QueryResponse.Item[0].Id
              itemdesc = item.name;
            }
            //console.log("Product", prodid);
            line_items.push({
              "Description": itemdesc,
              "DetailType": "SalesItemLineDetail",
              "Amount": item.total_price,
              "SalesItemLineDetail": {
                "TaxCodeRef": {
                  "value": taxstring
                },
                "ItemRef": {
                  "name": item.final_sku,
                  "value": prodid,
                },
                "Qty": item.quantity,
                "UnitPrice": item.unit_price,
                "ClassRef": {
                  "value": classid,
                  "name": classname
                },
              }
            });
            // add line item
            //itemnum = customer.Id;

            resolve();
          });
        }); // --promise
      }

      if (detail.shipment.cost > 0) {
        line_items.push({
          "Description": "Shipping",
          "DetailType": "SalesItemLineDetail",
          "Amount": detail.shipment.cost,
          "SalesItemLineDetail": {
            "TaxCodeRef": {
              "value": shiptax
            },
            "ItemRef": {
              "name": "Freight",
              "value": freightid,
            },
            "Qty": 1,
            "UnitPrice": detail.shipment.cost,
            "ClassRef": {
              "value": classid,
              "name": classname
            },
          }
        });
      }
      //console.log("Item List", line_items);

      //discounts
      if (detail.coupons_adjustments.length > 0) {
        var discount = 0;
        for (var adj of detail.coupons_adjustments) {
          discount += adj.amount;
          console.log("discount  ", discount, adj)
        }

        line_items.push({
          "DetailType": "DiscountLineDetail",
          "Amount": discount,
          "DiscountLineDetail": {
            "DiscountAccountRef": {
              "name": "Sales Discounts",
              "value": "5953"
            },
            "PercentBased": false
          }
        });

      }

      //create invoice

      var params_inv = {
        "DocNumber": qboorder,
        "ApplyTaxAfterDiscount": false,
        "BillAddr": {
          "CountrySubDivisionCode": detail.billing_address.state,
          "City": detail.billing_address.city,
          "PostalCode": detail.billing_address.zip,
          "Line1": detail.billing_contact.first_name + " " + detail.billing_contact.last_name,
          "Line2": detail.billing_address.first_address,
          "Line3": detail.billing_address.second_address,
          "Country": "USA"
        },
        "TxnDate": (new Date(detail.created_at)).toISOString().split('T')[0],
        "TotalAmt": detail.billing_contact.item_total,
        "CustomerRef": {
          "name": customer,
          "value": custnum
        },
        "ShipAddr": {
          "City": detail.shipping_address.city,
          "Line1": detail.billing_contact.first_name + " " + detail.billing_contact.last_name,
          "Line2": detail.shipping_address.first_address,
          "Line3": detail.shipping_address.second_address,
          "PostalCode": detail.shipping_address.zip,
          "CountrySubDivisionCode": detail.shipping_address.state,
        },
        "DueDate": (new Date(detail.created_at)).toISOString().split('T')[0],
        "PrintStatus": "NeedToPrint",
        "Deposit": 0,
        "DepartmentRef": {
          "name": locationname,
          "value": locationid,
        },
        "EmailStatus": "NotSet",
        "Line": line_items,
        "ApplyTaxAfterDiscount": false,
        "BillEmail": {
          "Address": detail.customer_email
        },
      }; //- end invoice json
      if (taxamt == 0) {
        params_inv.TxnTaxDetail = {
          "TxnTaxCodeRef": {
            "value": "152"
          },
          "TotalTax": taxamt,
          // "TaxLine": [
          //   {
          //     "Amount": taxamt,
          //     "DetailType": "TaxLineDetail",
          //     "TaxLineDetail": {
          //       "TaxRateRef": {
          //         "value": "152"
          //       },
          //       "PercentBased": false,
          //     }
          //   }
          // ]
        }
      }; //- end invoice json
      console.log("Invoice:  ", params_inv);
      // create if has items and no errors
      if (line_items.length > 0 && errors.length == 0) {
        await new Promise(async (resolve, reject) => {
          await qbo.createInvoice(params_inv, async function (err, invoice) {
            if (err) {
              console.log("error", err);
              console.log("error", err.Fault.Error[0]);
              errors.push({ message: err.Fault.Error[0] })
            }
            //console.log("New Invoice", invoice);
            newinvoice = invoice;
            resolve();
          });
        }); // --promise
      }
      //create matching payment
      var payment =
      {
        "TotalAmt": newinvoice.TotalAmt,
        "CustomerRef": {
          "value": newinvoice.CustomerRef.value
        },
        "TxnDate": newinvoice.TxnDate,
        "PrivateNote": newinvoice.DocNumber,
        "Line": [
          {
            "Amount": newinvoice.TotalAmt,
            "LinkedTxn": [
              {
                "TxnId": newinvoice.Id,
                "TxnType": "Invoice"
              }
            ]
          }
        ]
      }
      await new Promise(async (resolve, reject) => {
        await qbo.createPayment(payment, async function (_, payment) {
          //console.log("new Payment", payment[0]);
        });
        resolve();
      });
      //update db
      if (errors.length == 0 && issues.length > 0) {
        await updatetxstatus(req, order, "Issue", runkey, errors, issues).then().catch(error => {
          console.log(error);
        });
      } else if (errors.length > 0) {
        await updatetxstatus(req, order, "Error", runkey, errors, issues).then().catch(error => {
          console.log(error);
        });
      } else {
        await updatetxstatus(req, order, "Done", runkey, [], []).then().catch(error => {
          console.log(error);
        });
      }

      // update bs side
      var updateurl = "https://" + req.query.domain + ".mybrightsites.com/api/v2.3.0/orders/" + order + "?token=" + req.query.token;
      console.log("updateurl: ", updateurl);
      axios.put(updateurl, { "order": { "note": "Imported to QB" } })
          .then(response => {
          });
      //console.log("done update");
      //await new Promise(resolve => setTimeout(resolve, 1000));
      index++;
      res.write("event: progress\n");
      res.write("data: " + index.toString() + "\n\n");

    };
    //update site db
    // if (siteerrors.length == 0 && issues.length > 0) {
    //   await updatesitestatus(req, "TX Issue", siteerrors, runkey).then().catch(error => {
    //     console.log(error);
    //   });
    // } else if (siteerrors.length > 0) {
    //   await updatesitestatus(req, "Error", siteerrors, runkey).then().catch(error => {
    //     console.log(error);
    //   });
    // } else {
    //   await updatesitestatus(req, "Done", [], runkey).then().catch(error => {
    //     console.log(error);
    //   });
    // }
    await updatesitestatus(req, "Done", [], runkey).then().catch(error => {
      console.log(error);
    })

    resolve();
  }).then(() => {
    //console.log("Sending redirect");
    res.write("event: goredirect\n");
    //res.write("data: " + "'https://dfp-qbo.btrsoftware.com:3001/orders?site_name=" +
    //  req.query.sitename + "&invoicepre=" + req.query.invoicepre + "&domain=" + req.query.domain + "&token=" + req.query.token + "'" + "\n\n");

    res.write("data: " + "https://dfp-qbo.btrsoftware.com:3001/run-result\n\n");
  }).catch(error => {
    console.log(error);
  });

});

app.get('/update/add', async function (req, res) {
  var itemerrors = [];
  res.writeHead(200, { 'Content-Type': 'text/event-stream' });

  const items = await CSVToJSON().fromFile('files/add_to_update.csv');

  //console.log("Items to update", items);
  var i = 0;
  var a = 0;
  var output = [];
  await new Promise(async (resolve, reject) => {
    for (const mitem of items) {
      //console.log(item.name);
      if (i > 30) {
        await new Promise(resolve => setTimeout(resolve, 1000));
        i = 0;
      }
      if (a > 400) {
        console.log("Waiting");
        await new Promise(resolve => setTimeout(resolve, 60000));
        a = 0;
      }
      i++;
      a++;
      console.log("Getting item: ", mitem.product);
      var loopitems = [];
      var itema = null;
      var itemi = null;
      await new Promise(async (resolve, reject) => {
        await qbo.findItems({ Name: mitem.product, active: false }, async function (_, items) {
          //console.log("Got item");
          if (items[0] != undefined) {
            loopitems = items[0].QueryResponse.Item;
          } else {
            console.log(items.fault.error);
            console.log("No results");
          }
          resolve();
        }).then(() => {

          //console.log("end find");

        }).catch((error) => {
          console.log("Error", items);
        });
      });
      for (var item of loopitems) {
        //make active

        console.log("activate item: ", item.Name);
        console.log("ID: ", item.Id);
        console.log("Existing Qty: ", item.QtyOnHand);
        console.log("Update Qty: ", mitem.total);
        //inactivate and create new
        var updateitem = {
          Id: item.Id,
          SyncToken: item.SyncToken,
          Active: true
        };
        console.log("Updateitem ", updateitem);
        await new Promise(async (resolve, reject) => {
          await qbo.updateItem(updateitem, async function (err, result) {
            if (err) console.log(err.Fault.Error[0])
            else {
              output.push({ name: item.Name, result: "updated" });
              console.log("updated: ", result);
              console.log("end update");
              resolve();
            }
          });
        });

        //update qty
        await new Promise(async (resolve, reject) => {
          await qbo.findItems({ Id: item.Id }, async function (_, items) {
            //console.log("Got item");
            if (items[0] != undefined) {
              itema = items[0].QueryResponse.Item[0];
            } else {
              console.log(items.fault.error);
              console.log("No results");
            }
            resolve();
          }).then(() => {

            //console.log("end find");

          }).catch((error) => {
            console.log("Error", items);
          });
        });

        console.log("update item: ", itema.Name);
        console.log("ID: ", itema.Id);
        console.log("Existing Qty: ", itema.QtyOnHand);
        console.log("Update Qty: ", mitem.total);
        //inactivate and create new
        var updateitem = {
          Id: itema.Id,
          SyncToken: itema.SyncToken,
          QtyOnHand: 0,
          InvStartDate: '2021-01-02'
        };
        console.log("Updateitem ", updateitem);
        await new Promise(async (resolve, reject) => {
          await qbo.updateItem(updateitem, async function (err, result) {
            if (err) console.log(err.Fault.Error[0])
            else {
              output.push({ name: item.Name, result: "updated" });
              console.log("updated: ", result);
              console.log("end update");
              resolve();
            }
          });
        });
        //deactivate 
        await new Promise(async (resolve, reject) => {
          await qbo.findItems({ Id: item.Id }, async function (_, items) {
            //console.log("Got item");
            if (items[0] != undefined) {
              itemi = items[0].QueryResponse.Item[0];
            } else {
              console.log(items.fault.error);
              console.log("No results");
            }
            resolve();
          }).then(() => {

            //console.log("end find");

          }).catch((error) => {
            console.log("Error", items);
          });
        });

        console.log("intivate item: ", itemi.Name);
        console.log("ID: ", itemi.Id);
        console.log("Existing Qty: ", itemi.QtyOnHand);
        console.log("Update Qty: ", mitem.total);
        //inactivate and create new
        var updateitem = {
          Id: itemi.Id,
          SyncToken: itemi.SyncToken,
          Active: false
        };
        console.log("Updateitem ", updateitem);
        await new Promise(async (resolve, reject) => {
          await qbo.updateItem(updateitem, async function (err, result) {
            if (err) console.log(err.Fault.Error[0])
            else {
              output.push({ name: item.Name, result: "updated" });
              console.log("updated: ", result);
              console.log("end update");
              resolve();
            }
          });
        });

      }
    } //end for
    console.log("end for");
    resolve();
  }).then(() => {
    const csvData = csvjson.toCSV(output, {
      headers: 'key'
    });
    writeFile('./files/output.csv', csvData, (err) => {
      if (err) {
        console.log(err);
        throw new Error(err);
      }
      console.log('Success!');
      res.send();
    });
  });

});

app.get('/update/qty', async function (req, res) {
  var itemerrors = [];
  res.writeHead(200, { 'Content-Type': 'text/event-stream' });

  const items = await CSVToJSON().fromFile('files/qty_to_update.csv');

  //console.log("Items to update", items);
  var i = 0;
  var a = 0;
  var output = [];
  await new Promise(async (resolve, reject) => {
    for (const mitem of items) {
      //console.log(item.name);
      if (i > 30) {
        await new Promise(resolve => setTimeout(resolve, 1000));
        i = 0;
      }
      if (a > 400) {
        console.log("Waiting");
        await new Promise(resolve => setTimeout(resolve, 60000));
        a = 0;
      }
      i++;
      a++;
      console.log("Getting item: ", mitem.product);
      var loopitems = [];
      await new Promise(async (resolve, reject) => {
        await qbo.findItems({ Name: mitem.product }, async function (_, items) {
          //console.log("Got item");
          if (items[0] != undefined) {
            loopitems = items[0].QueryResponse.Item;
          } else {
            console.log(items.fault.error);
            console.log("No results");
          }
          resolve();
        }).then(() => {

          //console.log("end find");

        }).catch((error) => {
          console.log("Error", items);
        });
      });
      for (var item of loopitems) {
        //update qty

        console.log("update item: ", item.Name);
        console.log("ID: ", item.Id);
        console.log("Existing Qty: ", item.QtyOnHand);
        console.log("Update Qty: ", mitem.total);
        //inactivate and create new
        var updateitem = {
          Id: item.Id,
          SyncToken: item.SyncToken,
          QtyOnHand: mitem.total,
          InvStartDate: '2022-03-14'
        };
        console.log("Updateitem ", updateitem);
        await new Promise(async (resolve, reject) => {
          await qbo.updateItem(updateitem, async function (err, result) {
            if (err) console.log(err.Fault.Error[0])
            else {
              output.push({ name: item.Name, result: "updated" });
              console.log("updated: ", result);
              console.log("end update");
              resolve();
            }
          });
        });


      }
    } //end for
    console.log("end for");
    resolve();
  }).then(() => {
    const csvData = csvjson.toCSV(output, {
      headers: 'key'
    });
    writeFile('./files/output.csv', csvData, (err) => {
      if (err) {
        console.log(err);
        throw new Error(err);
      }
      console.log('Success!');
      res.send();
    });
  });

});

app.get('/update/account', async function (req, res) {
  var itemerrors = [];
  res.writeHead(200, { 'Content-Type': 'text/event-stream' });

  const items = await CSVToJSON().fromFile('files/set_to_non_and _0qty.csv');

  //console.log("Items to update", items);
  var i = 0;
  var a = 0;
  var output = [];
  await new Promise(async (resolve, reject) => {
    for (const mitem of items) {
      //console.log(item.name);
      if (i > 30) {
        await new Promise(resolve => setTimeout(resolve, 1000));
        i = 0;
      }
      if (a > 400) {
        console.log("Waiting");
        await new Promise(resolve => setTimeout(resolve, 60000));
        a = 0;
      }
      i++;
      a++;
      console.log("Getting item: ", mitem.name);
      var loopitems = [];
      await new Promise(async (resolve, reject) => {
        await qbo.findItems({ Name: mitem.name }, async function (_, items) {
          //console.log("Got item");
          if (items[0] != undefined) {
            loopitems = items[0].QueryResponse.Item;
          } else {
            console.log(items.fault.error);
            console.log("No results");
          }
          resolve();
        }).then(() => {

          //console.log("end find");

        }).catch((error) => {
          console.log("Error", items);
        });
      });
      for (var item of loopitems) {
        //await items[0].QueryResponse.Item.forEach(async function (item) {

        if ((item.ExpenseAccountRef.value == 5793 && item.IncomeAccountRef.value != 5791) ||
          (item.ExpenseAccountRef.value != 5793 && item.IncomeAccountRef.value == 5791)
        ) {
          console.log("update item: ", item.Name);
          console.log("Income Account: ", item.IncomeAccountRef);
          console.log("Expense Account: ", item.ExpenseAccountRef);
          console.log("ID: ", item.Id);
          //inactivate and create new
          var updateitem = {
            Id: item.Id,
            SyncToken: item.SyncToken,
            sparse: true,
            IncomeAccountRef: { value: 5791 },
            ExpenseAccountRef: { value: 5793 }
          };

          await new Promise(async (resolve, reject) => {
            await qbo.updateItem(updateitem, async function (err, result) {
              if (err) console.log(err.Fault.Error[0])
              else {
                output.push({ name: item.Name, result: "updated" });
                console.log("updated: ", item.Name);
                console.log("end update");
                resolve();
              }
            });
          });
        } else {
          console.log("Not updating: ", mitem.name);
        }

      }
    } //end for
    console.log("end for");
    resolve();
  }).then(() => {
    const csvData = csvjson.toCSV(output, {
      headers: 'key'
    });
    writeFile('./files/output.csv', csvData, (err) => {
      if (err) {
        console.log(err); // Do something to handle the error or just throw it
        throw new Error(err);
      }
      console.log('Success!');
    });
  });

});

app.get('/update/items', async function (req, res) {
  var itemerrors = [];
  res.writeHead(200, { 'Content-Type': 'text/event-stream' });



  //get item list

  //const items = await CSVToJSON().fromFile('files/set_to_dnt.csv');
  const items = await CSVToJSON().fromFile('files/set_to_non_and _0qty.csv');

  console.log("Items to update", items);
  var i = 0;
  var a = 0;
  var output = [];
  await new Promise(async (resolve, reject) => {
    for (const mitem of items) {
      //console.log(item.name);
      if (i > 30) {
        await new Promise(resolve => setTimeout(resolve, 1000));
        i = 0;
      }
      if (a > 400) {
        console.log("Waiting");
        await new Promise(resolve => setTimeout(resolve, 60000));
        a = 0;
      }
      i++;
      a++;
      console.log("Getting item: ", mitem.name);
      var loopitems = [];
      await new Promise(async (resolve, reject) => {
        await qbo.findItems({ Name: mitem.name }, async function (_, items) {
          console.log("Got item");
          if (items[0] != undefined) {
            loopitems = items[0].QueryResponse.Item;
          } else {
            console.log(items.fault.error);
            console.log("No results");
          }
          resolve();
        }).then(() => {

          console.log("end find");

        }).catch((error) => {
          console.log("Error", items);
        });
      });
      for (var item of loopitems) {
        //await items[0].QueryResponse.Item.forEach(async function (item) {
        if (item.Type == "NonInventory") {
          console.log("Not updating: ", item.Name);
          output.push({ name: item.Name, result: "not updated", oldqty: 0 });
        }
        if (item.Type != "NonInventory" || item.TrackQtyOnHand != false) {
          console.log("update item: ", item.Name);
          console.log("update date: ", item.MetaData.LastUpdatedTime);
          console.log("create date: ", item.MetaData.CreateTime);
          console.log("ID: ", item.Id);
          console.log("Type: ", item.Type);
          console.log("TrackQtyOnHand: ", item.TrackQtyOnHand);

          if (item.Type == "Inventory") {
            if (item.MetaData.CreateTime > new Date('January 3, 2022 00:00:00')) {
              console.log("Not updating new: ", item.Name);
              output.push({ name: item.Name, result: "not updated new", oldqty: 0 });
            } else {
              //inactivate and create new
              var updateitem = {
                Id: item.Id,
                SyncToken: item.SyncToken,
                sparse: true,
                Active: false
              };
              var oldqty = 0;
              if (item.QtyOnHand != undefined) {
                oldqty = item.QtyOnHand;
              }
              await new Promise(async (resolve, reject) => {
                await qbo.updateItem(updateitem, async function (err, result) {
                  if (err) console.log(err.Fault.Error[0])
                  else {
                    console.log("deleted: ", result);
                    console.log("end update");
                    resolve();
                  }
                });
              }).then(async () => {
                item.Type = "NonInventory";
                item.TrackQtyOnHand = false;
                delete item.SyncToken;
                delete item.Id;
                delete item.MetaData;
                delete item.QtyOnHand;
                delete item.InvStartDate;
                await new Promise(async (resolve, reject) => {
                  console.log("Creating item: ", item);
                  await qbo.createItem(item, async function (err, result) {
                    if (err) console.log(err.Fault.Error[0])
                    else {
                      console.log("created: ", result);
                      output.push({ name: item.Name, result: "updated", oldqty: oldqty });
                      console.log("end create");
                      resolve();
                    }

                  });
                });
              });

            }
          }
        }

      }



    } //end for
    console.log("end for");
    resolve();
  }).then(() => {
    const csvData = csvjson.toCSV(output, {
      headers: 'key'
    });
    writeFile('./files/output.csv', csvData, (err) => {
      if (err) {
        console.log(err); // Do something to handle the error or just throw it
        throw new Error(err);
      }
      console.log('Success!');
    });
  });

});


app.get('/update/bills', async function (req, res) {
  var itemerrors = [];
  res.writeHead(200, { 'Content-Type': 'text/event-stream' });

  //get item list

  //const items = await CSVToJSON().fromFile('files/set_to_dnt.csv');
  //const items = await CSVToJSON().fromFile('files/set_to_non_and _0qty.csv');

  //console.log("Items to update", items);
  var i = 0;
  var a = 0;
  var output = [];
  await new Promise(async (resolve, reject) => {

    //console.log(item.name);
    if (i > 30) {
      await new Promise(resolve => setTimeout(resolve, 1000));
      i = 0;
    }
    if (a > 400) {
      console.log("Waiting");
      await new Promise(resolve => setTimeout(resolve, 60000));
      a = 0;
    }
    i++;
    a++;
    console.log("Getting Bills: ");
    var loopitems = [];
    await new Promise(async (resolve, reject) => {
      await qbo.findBills({}, async function (err, bills) {
        if (err) {
          console.log(err);
        } else {
          //console.log("Got bills", bills[0].QueryResponse.Bill);
        }

        if (bills[0] != undefined) {
          loopitems = bills[0].QueryResponse.Bill;
        } else {
          console.log(bills.fault.error);
          console.log("No results");
        }
        resolve();
      });
    });
    for (var bill of loopitems) {
      var updatebill = false;

      var linenum = 0;
      for (var line of bill.Line) {

        //check for deleted
        if (line.ItemBasedExpenseLineDetail != undefined) {
          if (line.ItemBasedExpenseLineDetail.ItemRef.name.includes("(deleted)")) {
            updatebill = true;

            var newitem = line.ItemBasedExpenseLineDetail.ItemRef.name.replace(" (deleted)", "")
            //get item
            var itemnum = 0;
            await new Promise(async (resolve, reject) => {
              console.log("Getting", newitem);
              await qbo.findItems({ Name: newitem }, async function (err, products) {
                //console.log(products);
                if (err) {
                  console.log(err);
                } else {
                  if (products[0].QueryResponse != undefined) {
                    itemnum = products[0].QueryResponse.Item[0].Id;
                    console.log("Item id", itemnum);
                  } else {
                    console.log(" Item Missing [" + newitem + "]");
                  }
                }
                resolve();
              });

            });

            console.log("Replacing with", itemnum, newitem, linenum);
            //update line
            line.ItemBasedExpenseLineDetail.ItemRef.name = newitem;
            line.ItemBasedExpenseLineDetail.ItemRef.value = itemnum;
            bill.Line[linenum] = line;
          }
        }
        linenum++;
      }
      if (updatebill) {
        bill.sparse = true;

        console.log("updating bill", JSON.stringify(bill, null, 4));
        output.push({ id: bill.Id, bill: bill.DocNumber, date: bill.DueDate, result: "updated" });

        //write update
        await new Promise(async (resolve, reject) => {
          await qbo.updateBill(bill, async function (err, rbill) {
            if (err) {
              console.log("error", err);
              console.log("error", err.Fault.Error[0]);
            }
            console.log("Updated bill", rbill);
            resolve();
          });
        }); // --promise

      } else {
        output.push({ id: bill.Id, bill: bill.DocNumber, date: bill.DueDate, result: "not" });

      }


    }



    resolve();
  }).then(() => {
    const csvData = csvjson.toCSV(output, {
      headers: 'key'
    });
    writeFile('./files/billsout.csv', csvData, (err) => {
      if (err) {
        console.log(err); // Do something to handle the error or just throw it
        throw new Error(err);
      }
      console.log('Success!');
    });
  });

});

app.get('/update/pos', async function (req, res) {
  var itemerrors = [];
  res.writeHead(200, { 'Content-Type': 'text/event-stream' });

  //get item list

  //const items = await CSVToJSON().fromFile('files/set_to_dnt.csv');
  //const items = await CSVToJSON().fromFile('files/set_to_non_and _0qty.csv');

  //console.log("Items to update", items);
  var i = 0;
  var a = 0;
  var output = [];
  await new Promise(async (resolve, reject) => {

    //console.log(item.name);
    if (i > 30) {
      await new Promise(resolve => setTimeout(resolve, 1000));
      i = 0;
    }
    if (a > 400) {
      console.log("Waiting");
      await new Promise(resolve => setTimeout(resolve, 60000));
      a = 0;
    }
    i++;
    a++;
    console.log("Getting POs: ");
    var loopitems = [];
    await new Promise(async (resolve, reject) => {
      await qbo.findPurchaseOrders({}, async function (err, pos) {
        if (err) {
          console.log(err);
        } else {
          //console.log("Got POs", pos[0].QueryResponse.PurchaseOrder);
        }

        if (pos[0] != undefined) {
          loopitems = pos[0].QueryResponse.PurchaseOrder;
        } else {
          console.log(pos.fault.error);
          console.log("No results");
        }
        resolve();
      });
    });
    for (var po of loopitems) {
      var updatepo = false;
      //console.log("Lines", po.Line);
      var linenum = 0;
      for (var line of po.Line) {

        //check for deleted
        if (line.ItemBasedExpenseLineDetail != undefined) {
          if (line.ItemBasedExpenseLineDetail.ItemRef.name.includes("(deleted)")) {
            updatepo = true;

            var newitem = line.ItemBasedExpenseLineDetail.ItemRef.name.replace(" (deleted)", "")
            //get item
            var itemnum = 0;
            await new Promise(async (resolve, reject) => {
              console.log("Getting", newitem);
              await qbo.findItems({ Name: newitem }, async function (err, products) {
                //console.log(products);
                if (err) {
                  console.log(err);
                } else {
                  if (products[0].QueryResponse != undefined) {
                    itemnum = products[0].QueryResponse.Item[0].Id;
                    console.log("Item id", itemnum);
                  } else {
                    console.log(" Item Missing [" + newitem + "]");
                  }
                }
                resolve();
              });

            });

            console.log("Replacing with", itemnum, newitem, linenum);
            //update line
            line.ItemBasedExpenseLineDetail.ItemRef.name = newitem;
            line.ItemBasedExpenseLineDetail.ItemRef.value = itemnum;
            po.Line[linenum] = line;
          }
        }
        linenum++;
      }
      if (updatepo) {
        po.sparse = true;

        console.log("updating bill", JSON.stringify(po, null, 4));
        output.push({ id: po.Id, po: po.DocNumber, date: po.DueDate, result: "updated" });

        //write update
        /*await new Promise(async (resolve, reject) => {
          await qbo.updatePurchaseOrder(po, async function (err,rpo) {
            if (err) {
              console.log("error", err);
              console.log("error", err.Fault.Error[0]);
            }
            console.log("Updated bill", rpo);
            resolve();
          });
        }); // --promise */

      } else {
        output.push({ id: po.Id, po: po.DocNumber, date: po.DueDate, result: "not" });

      }

    }



    resolve();
  }).then(() => {
    const csvData = csvjson.toCSV(output, {
      headers: 'key'
    });
    writeFile('./files/posout.csv', csvData, (err) => {
      if (err) {
        console.log(err); // Do something to handle the error or just throw it
        throw new Error(err);
      }
      console.log('Success!');
    });
  });

});

app.get('/bs-aig', function (req, res) {
  axios.get('https://sung.mybrightsites.com/api/v2.3.0/orders?token=Q4rhJ4-PqaMwavz1zp3N&status=new')
    .then(response => {
      res.send(response.data.orders);
    })
    .catch(error => {
      console.log(error);
    });
});

app.get('/callback', function (req, res) {
  var auth = (new Buffer(consumerKey + ':' + consumerSecret).toString('base64'));

  var postBody = {
    url: 'https://oauth.platform.intuit.com/oauth2/v1/tokens/bearer',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/x-www-form-urlencoded',
      Authorization: 'Basic ' + auth,
    },
    form: {
      grant_type: 'authorization_code',
      code: req.query.code,
      redirect_uri: 'https://dfp-qbo.btrsoftware.com:' + port + '/callback/'  //Make sure this path matches entry in application dashboard
    }
  };

  request.post(postBody, function (e, r, data) {
    accessToken = JSON.parse(r.body);
    console.log(accessToken);
    console.log("realmId", req.query.realmId);

    // save the access token somewhere on behalf of the logged in user
    qbo = new QuickBooks(consumerKey,
      consumerSecret,
      accessToken.access_token, /* oAuth access token */
      false, /* no token secret for oAuth 2.0 */
      req.query.realmId,
      false, /* use a sandbox account */
      false, /* turn debugging on */
      4, /* minor version */
      '2.0', /* oauth version */
      accessToken.refresh_token /* refresh token */);

    //qbo.findTaxCodes( async function (_, taxes) {
    //  console.log("taxes", taxes[0].QueryResponse);
    //taxes[0].QueryResponse.Item.forEach(function (account) {
    //console.log(account);
    //});
    //}); 
    /* qbo.findInvoices({DocNumber:"bec231"}, async function (_, taxes) {
      console.log("taxes", taxes[0].QueryResponse.Invoice[0].TxnTaxDetail);
      //taxes[0].QueryResponse.Invoice.Line.forEach(function (account) {
        //console.log(account);
      //});
    }); */

  });

  res.send('<!DOCTYPE html><html lang="en"><head></head><body><script>window.opener.location= `/site-list`; window.close();</script></body></html>');
});

